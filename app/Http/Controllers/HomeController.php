<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('home');
    }

    public function about(Request $request)
    {
        return view('about');
    }

    public function contact(Request $request)
    {
        return view('contact');
    }

    public function discography(Request $request)
    {
        return view('discography');
    }

    public function admin(Request $request)
    {
        return view('admin');
    }

    public function test(Request $request)
    {
        return view('test');
    }
}

<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index']);
Route::get('/home', [HomeController::class, 'home']);
Route::get('/admin', [HomeController::class, 'admin']);
Route::get('/contact', [HomeController::class, 'contact']);
Route::get('/discography', [HomeController::class, 'discography']);
Route::get('/test', [HomeController::class, 'test']);


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
    <ul>
        <li><a href="/">Accueil</a></li>
        <li><a href="/about">A propos de moi</a></li>
        <li><a href="/contact">Page de contact</a></li>
        <li><a href="/discography">Discographie</a></li>
        <li><a href="/admin">Admin</a></li>
        <li><a href="/test">Test</a></li>
    </ul>
</body>
</html>
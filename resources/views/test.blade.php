<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test</title>
    <link rel="stylesheet" href="resources/css/app.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Poppins:wght@200&display=swap" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <header>
            <a href="#"><img src="../components/img/logo.jpg" alt="logotest"></a>
            <nav>
                <ul>
                    <li><a href="#" class="active">Accueil</li>
                    <li><a href="#">Discographie</li>                   
                    <li><a href="#">Projets</li>
                    <li><a href="#">A propos de moi</li>
                    <li><a href="#">Contact</li>         
                </ul>
            </nav>    
        </header>
    </div>
    <main>
        <div class="left-col">
            <h1> Title 1</h1>
            <p> Content 1 </p>
            <p> TEST </p>
        </div>
    </main>
</body>
</html>